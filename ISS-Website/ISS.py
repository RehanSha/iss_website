from flask import Flask, redirect, url_for, render_template
from geopy import location
from pip._vendor import requests
from geopy.geocoders import Nominatim

app = Flask(__name__)
nom = Nominatim(user_agent="IssLocate")

req = requests.get("http://api.open-notify.org/iss-now.json").json()


@app.route("/")
def home():
	return render_template("base.html")

@app.route("/find-the-iss")
def find_them():
	req = requests.get("http://api.open-notify.org/iss-now.json").json()
	Coords = str(req['iss_position']['latitude'] + ", " + req['iss_position']['longitude'])
	print(Coords)
	Iss_Location = nom.reverse(Coords)
	try:
		address = Iss_Location.address
		print(address)
		print(Coords)
	except Exception:
		address = "This location has no locatable address, so these coordinates may lead to an ocean or unamed regions."

	return render_template("Iss_Finder.html", LatAndLong = Coords, Place = address)
	
@app.errorhandler(404)
def fourOfour(error):
	return render_template('404.html')

@app.errorhandler(500)
def fivehundy(error):
	return render_template('500.html')

if __name__ == "__main__":
	app.run(port = 8080)

