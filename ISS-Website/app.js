const express = require("express");
const app = express();
app.use(express.static('static'));
const reverse = require('reverse-geocode')
const ISSAPIURL = "https://api.wheretheiss.at/v1/satellites/25544"
const fetch = require("node-fetch")
const fs = require("fs");
const { hostname } = require("os");


async function getData() {
    const response = await fetch(ISSAPIURL);
    const data = await response.json();
    const {latitude, longitude} = data;
    var Coords = latitude + ", " + longitude
    var Access = reverse.lookup(latitude, longitude, 'us')
    var ZIP = Access["zipcode"]
    var CITY = Access["city"]
    var STATE_ABBR = Access["state_abbr"]
    var DISTANCE = Access["distance"]
    if(Access["city"] == ''){
        CITY = "Uknown"
    }
    if(Access["zipcode"] == ''){
        ZIPCODE = "Uknown"
    }
    if(Access["state_abbr"] == ''){
        STATE_ABBR = "Uknown"
    }
    if(Access["distance"] == ''){
        DISTANCE = "Uknown"
    }
    var Address = ZIP + ", " + CITY + ", " + STATE_ABBR + ", which is  " + DISTANCE + " miles away"
    console.log(Access)
    fs.writeFileSync("static/DataPassage.txt", Address)
    fs.writeFileSync("static/DataPassage2.txt", Coords)
}


app.get("/", function (req, res) {
    res.sendFile(__dirname + "/Templates/base.html");
});
app.get("/find-the-iss", function (req, res) {
    getData()
        .catch(error =>{
            console.log(error)
            console.error(error)
        })
    res.sendFile(__dirname + "/Templates/Iss_Finder.html");
});
app.listen(3000, function () {});